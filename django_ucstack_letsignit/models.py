from django.db import models

from django_ucstack_models.django_ucstack_models.models import UcTenantSettings


class UcTenantSettingsLetsignit(models.Model):
    tenant = models.OneToOneField(UcTenantSettings, models.DO_NOTHING, unique=True)
    lsi_app_id = models.CharField(max_length=32, null=False, blank=False)
    lsi_app_secret = models.CharField(max_length=200, null=False, blank=False)

    class Meta:
        db_table = "uc_tenant_settings_letsignit"
